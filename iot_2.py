#!/usr/bin/env python

import numpy as np
#iris dataset 

url = 'https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data'

print(url)

print("iris_data = np.genfromtxt(url, delimiter="," , dtype=\"object\")")
iris_data = np.genfromtxt(url, delimiter="," , dtype="object")

isimler = ('sepallength','sepalwidth', 'petallength', 'petalwidth', 'species')
print(iris_data[0,0:])

print("sadece son sutun iris_data[:,4])")
print(iris_data[:,4])

print("float olarak veriyi alalım")
float_data = np.genfromtxt(url, delimiter="," , dtype="float" , usecols=[0,1,2,3])
sepallength = float_data[:,0]
sepalwidth = float_data[:,1]
petallength = float_data[:,2]
petalwidth = float_data[:,3]


print("np.mean(sepallength)")
print(np.mean(sepallength))
print("np.median(sepallength)")
print(np.median(sepallength))
print("np.std(sepallength)")
print(np.std(sepallength))

# normalization

# max , min

print("sepallength.max()")
print(sepallength.max())
Smax = sepallength.max()

print("sepallength.min()")
print(sepallength.min())
Smin = sepallength.min()

print("S = (sepallength-Smin)/(Smax-Smin)")
S = (sepallength-Smin)/(Smax-Smin)
print(S)


##############scientific notation#True:Decimal##############
##############scientific notation#False:Scientific##############
test = np.random.random([3,3])/1e3
print("np.set_printoptions(suppress=True)")
np.set_printoptions(suppress=True)
print(test)

print("np.set_printoptions(suppress=False)")
np.set_printoptions(suppress=False)
print(test)

#dtype=(int, float, int) - veri okurken uygun type ile alma
all_data = np.genfromtxt(url, delimiter="," , dtype=(float,float,float,float,object) , usecols=[0,1,2,3,4])
print(all_data)

sepal_length = float_data[:,0]
sepal_width = float_data[:,1]
petal_length = float_data[:,2]
petal_width = float_data[:,3]
print(sepal_length)
#matrix birlestirme üstüste - vertical
a = np.arange(10).reshape(2,5)
b = np.repeat(1,10).reshape(2,-1)
print(a)
print(b)
vstacked =np.vstack([a,b])
print(vstacked)

#matrix birleştirme yanyana - horizontal

hstacked = np.hstack([a,b])
print(hstacked)

print("vstacked = np.concatenate([a,b], axis = 0)")
stacked = np.concatenate([a,b], axis = 0)
print(stacked)

print("hstacked = np.concatenate([a,b], axis = 1)")
stacked = np.concatenate([a,b], axis = 1)
print(stacked)



corrcoef = np.corrcoef(sepal_length, sepal_width)
print(corrcoef)

print("x = np.stack((sepal_length, sepal_width), axis=0)")
x = np.stack((sepal_length, sepal_width), axis=0)
print("cov = np.cov(x)")
cov = np.cov(x)
print(cov)
print("cov = np.cov(sepal_length, sepal_width)")
cov = np.cov(sepal_length, sepal_width)
print(cov)

# petal length 1,5 dan büyük, 
a  = petal_length[petal_length>1.5]

print(a)
# sepal length 1,5 dan büyük, sepal_length 5 ten küçük
orta_iris_filter = (float_data[:,2]>1.5) & (float_data[:,2]<5)
ortairis = float_data[orta_iris_filter]
print(a)


# corelation 
print("corrcoef = np.corrcoef(sepal_length, sepal_width)")
corrcoef = np.corrcoef(sepal_length, sepal_width)[0,1]
print(corrcoef)

print("corrcoef = np.corrcoef(petal_length, petal_width)")
corrcoef = np.corrcoef(petal_length, petal_width)[0,1]
print(corrcoef)


print("float_data[3,3]=\"Nan\"")
float_data[3,3]="Nan"
print(float_data)

print("np.isnan(float_data)")
print(np.isnan(float_data))
print("np.where(np.isnan(float_data)==True)")
print(np.where(np.isnan(float_data)==True))

print("np.isnan(float_data).any()")
print(np.isnan(float_data).any())

print("np.count_zero(np.isnan(float_data))")
print(np.count_nonzero(np.isnan(float_data)))

# nan değerlerinin tamamını 0 ile değiştirelim
print("float_data[np.isnan(fload_data)]=0")
float_data[np.isnan(float_data)]=0
print(float_data)
print(np.isnan(float_data).any())

# sepal alanı ve petal alanı hesapla 5 ve 6 ncı kolonlara sonuçları ekle

alan_sepal = float_data[:,0]*float_data[:,1]
alan_petal = float_data[:,2]*float_data[:,3]

print(alan_sepal.reshape(-1,1))
sepal_alanli = np.concatenate((float_data, alan_sepal.reshape(-1,1)), axis=1)
petal_alanli = np.concatenate((sepal_alanli, alan_petal.reshape(-1,1)), axis=1)

print(petal_alanli)


print("corrcoef = np.corrcoef(petal_alanli[:,0], petal_alanli[:,4])")
corrcoef = np.corrcoef(petal_alanli[:,0], petal_alanli[:,4])[0,1]
print(corrcoef)
print("corrcoef = np.corrcoef(petal_alanli[:,1], petal_alanli[:,4])")
corrcoef = np.corrcoef(petal_alanli[:,1], petal_alanli[:,4])[0,1]
print(corrcoef)

print("corrcoef = np.corrcoef(petal_alanli[:,2], petal_alanli[:,5])")
corrcoef = np.corrcoef(petal_alanli[:,2], petal_alanli[:,5])[0,1]
print(corrcoef)
print("corrcoef = np.corrcoef(petal_alanli[:,3], petal_alanli[:,5])")
corrcoef = np.corrcoef(petal_alanli[:,3], petal_alanli[:,5])[0,1]
print(corrcoef)



print(iris_data)
print("iris_data[:,4]")
print(iris_data[:,4])
print("labelların ne oldugunu gor : np.unique(iris_data[:,4])") 
print(np.unique(iris_data[:,4]))
vals,counts = np.unique(iris_data[:,2], return_counts=True)
print("np.argmax(counts)")
print(np.argmax(counts))
print(vals[np.argmax(counts)])


# normal dağılım

print("normal_dagilim = np.random.normal(1, 50, 20)")
normal_dagilim = np.random.normal(1, 50, 20)
print(normal_dagilim)

print("np.clip(normal_dagilim, a_min=10, a_max=45)")
print(np.clip(normal_dagilim, a_min=10, a_max=45))

#outlier ları duzenleyelim, 10 dan kucuk olanları 10 ile 
# 45 ten büyük olanları 45 ile değiştirelim

normal_dagilim = np.random.normal(1, 50, 20)
print(normal_dagilim)
outliers = np.where(normal_dagilim<10, 10, normal_dagilim)
outliers = np.where(outliers>45, 45, outliers)
print(outliers)

# one hot encoding incele 
