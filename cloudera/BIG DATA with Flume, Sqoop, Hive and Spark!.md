# BIG DATA with Flume, Sqoop, Hive and Spark!

# 1 - Hadoop Introduction

## Lab: HDFS Komutları:
customer.txt dosyasını Linux işletim sistemine atalım (WinSCP)
Linux terminalde:
Put Komutu:  
``
hdfs dfs -put customer.txt /user/cloudera/practice``

List Komutu:  
``
hdfs dfs -ls /user/cloudera/practice
``  
(Not: Hue arayüzünden de dosyayı görelim. username:cloudera, password:cloudera)  
Cat Komutu:  
``
hdfs dfs -cat /user/cloudera/practice/customer.txt
``  
Text Komutu:  
(cat ile aynı işi yapar.)
``  
hdfs dfs -text /user/cloudera/practice/customer.txt
``  
Tail Komutu:  
(dosya sonunu gösteriyor)  
``
hdfs dfs -tail /user/cloudera/practice/customer.txt
``  

Mkdir Komutu:  
``hdfs dfs -mkdir /user/cloudera/new-practice``  
Move (mv) Komutu:  
``hdfs dfs -mv /user/cloudera/practice/customer.txt /user/cloudera/new-practice``  
Taşınıp taşınmadığını kontrol amaçlı:  
``hdfs dfs -ls /user/cloudera/new-practice``  
Change File Permissions (chmod) Komutu:  
``hdfs dfs -chmod 777 /user/cloudera/new-practice/customer.txt``  
Permissions Kontrol:  
``hdfs dfs -ls /user/cloudera/new-practice/customer.txt``  
Remove Folder (rm -r) Komutu:  
``hdfs dfs -rm -r /user/cloudera/new-practice``  
Get Komutu:  
(HDFS'den işletim sistemine dosya indirme)  
``hdfs dfs -get /user/cloudera/practice/customer.txt ~``  

# 2 - Sqoop IMPORT


Sqoop Introduction  
Veriyi dışarıdan HDFS'e alma işlemine Sqoop Import,  
HDFS'den dışarı gönderme işlemine Sqoop Export adı
veriliyor.
## Lab: MySQL'den HDFS'e Sqoop Import ile Veri Alma

Öncelikle Cloudera'da MySQL veritabanına bağlanalım:  
``mysql -u root -p cloudera -h localhost``  
retail_db veritabanına bağlanalım:  
``mysql> use retail_db;``  
Tabloları gösterelim:  
``mysql> show tables;``  
Bu tablolar arasındaki customers tablosunu HDFS tarafına getirmek istiyorum. Tablonun
MySQL'deki yapısına bakalım:  
``mysql> describe customers;``  
Yeni bir terminal açalım(Sqoop import tamamlanınca kayıt sayılarını karşılaştırmak
istediğim için MySQL penceresini kaybetmek istemiyorum.)
Linux komut satırında aşağıdaki sqoop import kodunu çalıştırarak veriyi MySQL'den
HDFS'e getirelim:  

## Simple Sqoop Import
```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root \
--password cloudera \
--table customers
```
Sqoop import'un sonunda belirtilen kayıt sayısının MySQL tarafıyla uyuşup uyuşmadığını
kontrol edelim.  
Not: Sqoop Import çalıştığında default olarak 1 değil, 4 paralel thread çalışıyor.  
Sqoop Import job'unun loglarını inceleyelim.   
Mapper'ların kaç kayıt işlediğini bulmaya
çalışalım.
MySQL'den HDFS'e tablo aktarımı gerçekleşti. Peki veriler nerede? Cloudera'da default
warehouse dizini,  **/user/cloudera/tablo_adı**
şeklinde oluyor. Dolayısıyla o dizine gidip gelen veriyi kontrol edelim:  
``hdfs dfs -ls /user/cloudera/customers``  
Default olarak 4 mapper çalıştığı için, 4 tane dosya var. Sqoop Import yaparken mapper
sayısını değiştirirsek, oluşan dosya sayısı da değişecektir.
Mapper sayısını değiştirerek import'u tekrar çalıştıralım. Ama öncelikle eski customers
dizinini silelim, yoksa aynı isimde klasör bulunduğu için hata verir:  
``hdfs dfs -rm -r /user/cloudera/customers``  
## Specifying Mapper

```
sqoop import \  
--connect jdbc:mysql://localhost/retail_db \  
--username root --password cloudera \  
--table customers \  
-m 2n 
```

Bu import bittikten sonra tekrar gelen verinin dizinini ve kaç dosya oluştuğunu kontrol
edelim:  
``hdfs dfs -ls /user/cloudera/customers``
Görüldüğü gibi, bu sefer 4 değil 2 dosya oluşmuş olacaktır.  
## Managing Target Directories
Diyelim ki default warehouse dizininden (/user/cloudera/) başka bir dizin kullanmak
istiyorum.

## Defining Warehouse Directory

```
sqoop import \  
--connect jdbc:mysql://localhost/retail_db \  
--username root --password cloudera \  
--table customers \  
--warehouse-dir /user/cloudera/new-warehouse
```

Şimdi bu yeni dizini kontrol edelim:  
``hdfs dfs -ls /user/cloudera/new-warehouse``  
Komutu çalıştırdığınızda, altında customers alt dizininin oluştuğunu göreceksiniz. Bu
dizini listelersek mapper dosyalarımızı görebileceğiz:  
``hdfs dfs -ls /user/cloudera/new-warehouse/customers``  
Sqoop Import'un işin sonunda tablo alt dizini oluşturmasını istemiyorsak, o zaman
warehouse-dir yerine target-dir kullanmamız gerekir:

## Defining Target Directory

```
sqoop import \  
--connect jdbc:mysql://localhost/retail_db \  
--username root --password cloudera \  
--table customers \  
--target-dir /user/cloudera/customer-new
```
Yeni dizini kontrol edelim. customers gibi bir alt dizin görmememiz gerekiyor.  
``hdfs dfs -ls /user/cloudera/customer-new``  
Aynı import komutunu tekrar çalıştıralım, bu sefer dizin mevcut olduğu için hata alacak:  

```
sqoop import \  
--connect jdbc:mysql://localhost/retail_db \  
--username root --password cloudera \  
--table customers \  
--target-dir /user/cloudera/customer-new
```

target-dir parametresini kullanırken, eski dizinin üzerine yazmasını istiyorsak, şu şekilde
yapıyoruz:

## Delete Target Directory If Already Exists  

```
sqoop import \  
--connect jdbc:mysql://localhost/retail_db \  
--username root --password cloudera \  
--table customers \  
--target-dir /user/cloudera/customer-new \  
--delete-target-dir
```

## Working with File Formats  
Şu ana kadar veriyi düz text formatında import ettik. Farklı formatlarda da import
edebiliriz
### Importing as Avro Files  
```
sqoop import \  
--connect jdbc:mysql://localhost/retail_db \  
--username root --password cloudera \  
--table customers \  
--target-dir /user/cloudera/customer-avro \  
--as-avrodatafile
```

Verinin atıldığı dizini listeleyelim. Avro formatındaki dosyaları göreceğiz:  
``hdfs dfs -ls /user/cloudera/customer-avro``

### Avro File Özellikleri:  
- Schema ve Data birlikte saklanır.
- Data, binary formattadır.
- Very efficient for data exchange in the Hadoop environment.

Sqoop Import sonucu oluşan avro dosyalarından birisini görüntüleyelim:  
``hdfs dfs -text /user/cloudera/customer_avro/part-m-00000.avro``

Dosya JSON formatında tutuluyor, ama aynı zamanda binary formatta sıkıştırılmış olarak
tutuluyor.
text komutuyla değil de tail komutuyla dosyayı görüntülemeye çalışırsak durum daha net
anlaşılır:  
``hdfs dfs -tail /user/cloudera/customer_avro/part-m-00000.avro``

Bu tür binary formatta tutulan dosyaları okurken, text komutu cat ve tail gibi komutlara
göre daha kullanışlı oluyor.
Şimdi de Parquet formatında import edelim:
 ### Importing as Parquet Files

```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root
--password cloudera \
--table customers \
--target-dir /user/cloudera/customer-parquet \
--as-parquetfile
```

Dizindeki dosyaları listeleyelim:  
``hdfs dfs -ls /user/cloudera/customer-parquet``  
Bu sefer dosyaları .parquet uzantısıyla göreceksiniz.
Parquet dosyalarının özelliği, columnar formatta tutulmaları ve çok iyi sıkıştırılmış
durumda olmaları.
tail komutuyla bir parquet dosyasının sonuna bakalım:
``hdfs dfs -tail /user/cloudera/customer-parquet/dosya_adi.parquet``  
tail ile sorunsuz okunabildiğine dikkat edelim.
text, avro, parquet dışında bir de sequence file formatında veriyi alabiliyoruz Sqoop
Import'dan

### Importing as Sequence Files:  

```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root
--password cloudera \
--table customers \
--target-dir /user/cloudera/customer-sequence \
--as-sequencefile
```

Dizindekileri listeleyelim:  
``hdfs dfs -ls /user/cloudera/customer-sequence``  
Bu sefer dosyalarda uzantı göremiyoruz, çünkü sequence file'lar aslında text file'lar.
Ama veri key,value şeklinde binary olarak tutuluyor.
Oluşan sequence file'lardan birisini görüntüleyelim:  
``hdfs dfs -tail /user/cloudera/customer-sequence/part-m-00000``

Dosyayı okuyamıyoruz, çünkü sequence file binary formatlı.

## Working with Different Compressions
Sqoop Import'da default sıkıştırma formatı **gzip** tir.  
###  Gzip Compressed

```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root --password cloudera \
--table customers \
--target-dir /user/cloudera/customer_gzip \
--compress
```

### Snappy Compressed
```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root
--password cloudera \
--table customers \
--target-dir /user/cloudera/customer_snappy \
--compress \
--compression-codec snappy

```

### Deflate Compressed
```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root
--password cloudera \
--table customers \
--target-dir /user/cloudera/customer_deflate \
--compress \
--compression-codec deflate
```  
### Bzip Compressed  

```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root --password cloudera \
--table customers \
--target-dir /user/cloudera/customer_bzip \
--compress \
--compression-codec bzip2
``` 
### Lz4 Compressed

```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root
--password cloudera \
--table customers \
--target-dir /user/cloudera/customer_lz4 \
--compress \
--compression-codec lz4
```  
Şimdi iki tane terminal açıp, sıkıştırma formatlarının dizinlerini sırayla listeleyelim:  
``hdfs dfs -ls /user/cloudera/customer_gzip``  
``hdfs dfs -ls /user/cloudera/customer_snappy``  
``hdfs dfs -ls /user/cloudera/customer_deflate``  
``hdfs dfs -ls /user/cloudera/customer_bzip``  
``hdfs dfs -ls /user/cloudera/customer_lz4``  

Büyüklüklerini karşılaştırdığımız zaman en büyük boyut snappy'de, en küçük boyut da
bzip de gözüküyor.
### Sıkıştırma ile ilgili Notlar:  
- gzip sıkıştırma oranı iyi ama Hadoop ortamında kullanmaktan kaçınılıyor, çünkü
bölünemeyen(unsplittable) dosyalar
- bzip'in sıkıştırma oranı en iyisi, ama çok yavaş ve çok fazla CPU gücü harcıyor.
- snappy'nin dosya boyutu en büyük olsa da, hem bölünebilir, hem de çok hızlı bir
şekilde compress ve decompress yapabiliyor.
- deflate, kullanımı yaygın değil o nedenle daha fazla hataya müsait (error prone)
- lz4'un sıkıştırma oranı snappy'den iyi, bölünebilir ve compress-decompress çok
hızlı.
Yukarıdaki sıkıştırma türlerine birlikte bakınca tüm alanlarda iyi olarak lz4 öne çıkıyor.

## Conditional Imports

Adı Mary olan müşterileri HDFS tarafına getirelim:  
```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root \
--password cloudera \
--table customers \
--target-dir /user/cloudera/customer-name-m \
--where "customer_fname='Mary'" 
```
Dizine gidelim:  
``hdfs dfs -ls /user/cloudera/customer-name-m``  
Dosyalardan birini açalım:  
``hdfs dfs -tail /user/cloudera/customer-name-m/part-m-0000``  
Bu örnekte de müşteri tablosunun tüm kolonlarını değil, first name, last name ve city
kolonlarını taşıyalım HDFS'e.  

## Selective Column Imports
```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root --password cloudera \
--table customers \
--target-dir /user/cloudera/customer-selected \
--columns "customer_fname,customer_lname,customer_city" 
```

Dizine gidip bir dosyayı açalım:  
``hdfs dfs -ls /user/cloudera/customer-selected/``  
``hdfs dfs -tail /user/cloudera/customer-selected/part-m-00000``  


## Using Query
Bu örnekte de tüm veriyi sorguyla alalım:  
```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root --password cloudera \
--target-dir /user/cloudera/customer-queries \
--query "Select * from customers where customer_id > 100 AND \$CONDITIONS" \
--split-by customer_id
```
ya da   
```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root --password cloudera \
--target-dir /user/cloudera/customer-queries \
--query 'Select * from customers where customer_id > 100 AND $CONDITIONS' \
--split-by customer_id
```

>fark:
query tek tırnak la yazılınca **$CONDITIONS**
çift tırnakla yazılınca **\\$CONDITIONS**

>primary key olmayan tablolarda split by kullanılarak mapper ın neye  göre böleceğini belirleyebiliyoruz. 

Using Query'de 3 önemli bölüm var:
1) Tablo adı olmayacak, çünkü zaten sorguda yazılıyor.
2) $CONDITIONS bölümü olacak, sqoop tarafından runtime'da değiştirebilmek için.
3) split-by bölümü olacak.

Dizine gidelim:  
``hdfs dfs -ls /user/cloudera/customer-queries``  
``hdfs dfs -cat /user/cloudera/customer-queries/part-m-00000 | head``  

Sorguda customer_id > 100 olsun demiştik. 
İlk kayıdın 101'den başladığına dikkat edelim.  
## Split-by and Boundary Queries
Split-by keywordündeki kolon, mapperlara eşit bir şekilde parçalama yapılırken referans
alınan kolondur.  
Tablonun primary key'i varsa, split-by'a gerek kalmadan otomatik olarak primary key
kullanılarak kayıtlar mapper'lara bölüştürülür.  
Tablonun primary key'i yoksa ve split-by belirtilmemişse, import hata verip işlemi keser.  
Örneğimizde, MySQL tarafında products tablosunu kullanacağız. Primary key olup
olmadığını kontrol edelim:  

`` mysql -u root -p cloudera -h localhost``  
>mysql> use retail_db;  
>mysql> describe products;  

Gördüğünüz gibi products tablosu üzerinde primary key yok. Şimdi split-by olmadan
sorgumuzu çalıştıralım. Mapperlara bölemeyeceği için hata verip kesmesi gerekiyor.  
```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root --password cloudera \
--table products \
--target-dir /user/cloudera/products_split \
```
ama var olduğunu gördük bu nedenle;   
``
create table copy_products as select * from products;
``  
diyerek yeni bir tablo oluşturduk ve haliyle yukarıdaki komutu düzelttik.  
```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root --password cloudera \
--table copy_products \
--target-dir /user/cloudera/products_split \
```
**HATA**  
 **Import failed: No primary key could be found for table copy_products. Please specify one with --split-by or perform a sequential import with '-m 1'**  
**HATA**  

Çalıştırıp sonucunu görelim.

## Split-by Query
Şimdi split-by ile çalıştıralım.  
```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root --password cloudera \
--table copy_products \
--target-dir /user/cloudera/products_split \
--split-by product_id \
--delete-target-dir
```
Son olarak, eğer biz müdahale etmezsek, sqoop min(product_id), max(product_id)
değerlerini alarak bölme sınırlarını hesaplar.  
Bu sınıra müdahale etmek istersek şöyle ediyoruz:  

## Boundary Query

```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root --password cloudera \
--table products \
--target-dir /user/cloudera/customer-boundary \
--boundary-query 'Select min(product_id),max(product_id)
from products where product_id>100' \
--split-by product_id
```
Sorguyu böyle çalıştırdığımızda ilk 100 kayıt alınmadan map edilmiş oluyor.  

## Field Delimiters

Kolon değerlerinin arasına istediğiniz değeri koymanızı sağlar, 
**--fields-terminated-by** parametresi ile set edilir. 
```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root --password cloudera \
--table customers \
--target-dir /user/cloudera/customers_delimited \
--fields-terminated-by "|" \
--delete-target-dir
```
Sonuç :  
``[cloudera@quickstart ~]$ hdfs dfs -tail /user/cloudera/customers_delimited/part-m-00000``
>Lynn|MA|01902
>3097|Douglas|Hanna|XXXXXXXXX|XXXXXXXXX|1112 Rustic Range|Caguas|PR|00725
>3098|Mary|Smith|XXXXXXXXX|XXXXXXXXX|8217 Fallen Panda Walk|Newburgh|NY|12550
>3099|Brittany|Copeland|XXXXXXXXX|XXXXXXXXX|5735 Round Beacon Terrace|Caguas|PR|00725
>3100|Mary|Smith|XXXXXXXXX|XXXXXXXXX|5436 Grand Hickory Farm|Huntington Park|CA|90255
>3101|George|Reyes|XXXXXXXXX|XXXXXXXXX|8702 Silver Apple Square|Mission Viejo|CA|92692
>3102|Ralph|Dixon|XXXXXXXXX|XXXXXXXXX|5633 Harvest Turnabout|Caguas|PR|00725
>3103|Mary|Wilkins|XXXXXXXXX|XXXXXXXXX|1213 Cotton Pike|Spring Valley|NY|10977
>3104|Megan|Smith|XXXXXXXXX|XXXXXXXXX|5292 Shady Pony Cape|Caguas|PR|00725
>3105|Mary|Stone|XXXXXXXXX|XXXXXXXXX|8510 Green River Acres|Toa Baja|PR|00949
>3106|Samantha|Smith|XXXXXXXXX|XXXXXXXXX|355 Cozy Square|Las Cruces|NM|88005
>3107|Tiffany|Estes|XXXXXXXXX|XXXXXXXXX|5182 Cotton Heath|Caguas|PR|00725
>3108|Mary|Smith|XXXXXXXXX|XXXXXXXXX|577 Rustic Nectar Row|Houston|TX|77083
>3109|Jack|James|XXXXXXXXX|XXXXXXXXX|5876 Burning Mall |Fort Worth|TX|76133

## Handling Null

**--null-string**  
**--null-non-string**  
önce verinin içerisinde null veri olmadığı için yeni null veriler oluşturalım:   
``mysql>
update copy_customers set customer_lname = NULL where customer_fname='Mary';
alter table copy_customers modify column customer_zipcode integer; ``
>Query OK, 12435 rows affected, 4741 warnings (0.14 sec)  
>Records: 12435  Duplicates: 0  Warnings: 0

``mysql>update copy_customers set customer_zipcode = NULL where customer_fname='Mary';``
>Query OK, 4741 rows affected, 4741 warnings (0.09 sec)  
>Rows matched: 4741  Changed: 4741  Warnings: 4741
```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root --password cloudera \
--table copy_customers \
--target-dir /user/cloudera/customers_with_null \
--fields-terminated-by "|" \
--null-string "alibaba" \
--null-non-string "999" \
--delete-target-dir \
--split-by customer_id
```

## Incremental Appends 

Mysql tarafından orders tablosunu HDFS tarafına atalım: 

### Simple Import 
```
sqoop import \  
--connect jdbc:mysql://localhost/retail_db \
--username root  --password cloudera \
--target-dir /user/cloudera/orders-incremental \  
--table orders
```
Şimdi bir MySQL penceresi açıp şu insertleri girelim: 
### Inserts

``insert into orders (order_id,order_date,order_status) values(100004,'2017-11-07 10:02:00','CLOSED');``  
``insert into orders (order_id,order_date,order_status) values(100005,'2017-11-07 10:02:00','CLOSED'); ``  
``insert into orders (order_id,order_date,order_status) values(100006,'2017-11-07 10:02:00','CLOSED'); ``  
Ve tekrar Sqoop Import başlatalım: 

### Incremental Append 

```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \  
--username root  --password cloudera \
--target-dir /user/cloudera/orders-incremental \
--table orders \
--incremental append \  
--check-column order_id \  
--last-value 100003 
```
(Not: incremental lastmodified ile de update takip edilebiliyor.) 
## SQOOP HIVE IMPORT 

Big Data ekosistemimizde Hive adında bir veritabanımız var. 

Hive, Facebook tarafından geliştirilmiş. 

Sorgulamalarını text dosyalar üzerinde MapReduce jobları yazarak yaptıkları bir dönemde Hive’ı geliştiriyorlar. 

HDFS üzerindeki text dosyalarını kullanarak Hive tabloları oluşturduğunuzda, bu Hive tabloları üzerinde direk olarak SQL yazarak çalışabiliyorsunuz. 

Sqoop’la daha önceki import örneklerimizde HDFS tarafında text dosyalara taşımıştık veriyi. Bu sefer, import ederken direk olarak Hive tablosu oluşturuyoruz: 

###   Hive Import

```
sqoop import \ 
--connect "jdbc:mysql://localhost/retail_db" \  
--username root  --password cloudera \  
--table customers \  
--hive-import \  
--create-hive-table \  
--hive-database default \  
--hive-table customer_mysql 
```
**HUE** arayüzünde Impala editoründe Hive tablolarını görmek için,   
``hive> invalidate metadata ``  
çalıştırın. 


Yeni bir Linux terminal açalım:   
 ``#hive ``  
komutuyla Hive’a bağlanıyorum. 

```
hive> show databases; 
hive> use default; 
hive> show tables; 
hive> select * from customer_mysql limit 5;
```
Tablonun oluşturulma scriptini görelim:   
``hive> show create table customer_mysql; ``  

Tablonun yapısını görelim: 

``hive> describe formatted customer_mysql; ``

Tablo hakkında çeşitli bilgiler görüyoruz. 

Location bölümünde tablonun veri kaynağı görünüyor, dikkat edelim. 

### Hive Import Change Field Delimiter 
field delimiter olarak tab boşluğu gözüküyor. Delimiter olarak ( | ) kullanarak yeniden import edelim.   

```
sqoop import \  
--connect "jdbc:mysql://localhost/retail_db" \  
--username root --password cloudera \  
--table customers \  
--fields-terminated-by '|' \  
--hive-import \  
--create-hive-table \  
--hive-database default \  
--hive-table customer_mysql_new 

```

Yeni tablomuzun yapısını Hive’da sorguladığımızda delimiter’ın pipe ( | ) olarak değiştiğini göreceğiz. 

`` hive> describe formatted customer_mysql_new ``


## Sqoop List Tables/Database 

Sqoop tarafından karşıdaki ilişkisel veritabanına ait bazı bilgileri, Sqoop komutlarıyla alabiliriz. 

### List Databases

```
sqoop list-databases \  
--connect jdbc:mysql://localhost \  
--username root  --password cloudera  
```

MySQL tarafındaki tüm veritabanlarının listelendiğini göreceğiz: 

Belirli bir veritabanındaki tabloları listeleyelim: 

### List Tables

```
sqoop list-tables \  
--connect jdbc:mysql://localhost/retail_db \  
--username root --password cloudera  
```

retail_db veritabanı altındaki tüm tabloların listelendiğini göreceğiz. 

Ve bir tablonun kolonlarını listeleyelim: 

### List Columns

```
sqoop eval \  
--connect jdbc:mysql://localhost/retail_db \  
--username root --password cloudera \ 
--query “describe customers” 
```

customers tablosunun kolonları listeleniyor. 
## Problems
### Problem-1 

#### Instructions:  

Connect to mySQL database using sqoop, import all orders that have order_status as COMPLETE Data  

#### Description:  

A mysql instance is running on the localhost. In that instance, you will find orders table that contains order’s data.  

>Installation: localhost  
Database name: retail_db  
Table name: Orders  
Username: root  
Password: cloudera  

#### Output Requirement:  

- Place the customer’s files in HDFS directory **"/user/cloudera/problem1/orders/parquetdata" ** 

 - Use parquet format with tab delimiter and snappy compression.  

 - Null values are represented as -1 for numbers and "NA" for string 

### Problem-2 

#### Instructions:  

Connect to mySQL database using sqoop, import all customers that lives in 'CA' state.  

#### Data Description:  

A mysql instance is running on the localhost node. In that instance, you will find customers table that contains customer’s data.  

>Installation: localhost  
Database name: retail_db  
Table name: Customers  
Username: root  
Password: cloudera  
#### Output Requirement:  
- Place the customers files in HDFS directory "/user/cloudera/problem1/customers_selected/avrodata"  

- Use avro format and snappy compression.  

- Load only customer_id,customer_fname,customer_lname,customer_state 

### Problem-3 

#### Instructions:  

Connect to mySQL database using sqoop, import all customers whose street name contains "Plaza" .  

#### Data Description:  

A mysql instance is running on the localhost node. In that instance you will find customers table that contains customers data.  

> Installation : localhost  
Database name: retail_db  
Table name: Customers  
Username: root  
Password: cloudera  

#### Output Requirement:  

- Place the customers files in HDFS directory **"/user/cloudera/problem1/customers/textdata"**  

- Save output in text format with fields seperated by a '*' and lines should be terminated by pipe  

- Load only "Customer id, Customer fname, Customer lname and Customer street name"  

#### Sample Output  

> 11942*Mary*Bernard*Tawny Fox Plaza|10480*Robert*Smith*Lost Horse Plaza|  
................................. 

## Solutions
###  Solution1  

```
sqoop import \  
--connect "jdbc:mysql://localhost/retail_db" \  
--username root  --password cloudera \  
--table orders \  
--compress  --compression-codec snappy \  
--target-dir /user/cloudera/problem1/orders/parquetdata \  
--null-non-string -1  --null-string "NA" \  
--fields-terminated-by "\t" \  
--where "order_status='COMPLETE'" \  
--as-parquetfile 
```

### Solution2  

```
sqoop import \  
--connect "jdbc:mysql://localhost /retail_db" \  
--username root  --password cloudera \  
--table customers \  
--compress \  
--compression-codec snappy \  
--target-dir /user/cloudera/problem1/customers_selected/avrodata \  
--where "customer_state='CA'" \  
--columns "customer_id,customer_fname,customer_lname,customer_state" \
--as-avrodatafile; 
```

###  Solution3  

```
sqoop import \  
--connect "jdbc:mysql://localhost/retail_db" \  
--username root  --password cloudera \  
--table customers \
--target-dir /user/cloudera/problem1/customers/textdata \  
--fields-terminated-by '*' \  
--lines-terminated-by '|' \  
--where "customer_street like '%Plaza%'" \  
--columns "customer_id,customer_fname,customer_lname,customer_street"
``` 

# 3 - Sqoop Export 

## Export from HDFS to MySQL 

Sqoop Export kullanarak HDFS’den MySQL’e veri taşıyalım: 

HDFS Location: **/user/cloudera/export-dir-cust** 

MySQL Table: **customer_exported** 

Null String: **EMPTY** 

Null Non-string: **0** 

Mapper: **3** 

```
sqoop export \ 
--connect jdbc:mysql://localhost/retail_db \
--username root --password cloudera \ 
--table customer_exported \ 
--export-dir /user/cloudera/export-dir-cust --input-fields-terminated-by ‘*’ \  --input-null-string ‘EMPTY’ \ 
--input-null-non-string 0 \
-m 3 
```

MySQL tarafında tabloyu kontrol edelim: 

``mysql> select * from cutomer_exported limit 5; ``

## Export from Hive to MySQL 

Sqoop Export kullanarak Hive’dan MySQL’e veri taşıyalım: 

Hive Database: **default** 

Hive Table: **product_hive** 

MySQL Database: **retail_db** 

MySQL Table: **product_exported** 

Önce Hive tarafında tabloyu kontrol edelim: 

``#hive``  
`` hive> select * from product_hive limit 5;  ``  
Şimdi scripti çalıştıralım:   

```
sqoop export \ 
--connect jdbc:mysql://localhost/retail_db \ 
--username root --password cloudera \ 
--table product_exported \ 
--hcatalog-table product_hive 
```


MySQL tarafında tabloyu kontrol edelim: 

``mysql> select * from product_exported limit 5; ``

``mysql> select count(*) from product_exported; ``

# 4 - Apache Hive 

 ## Hive Komutları 
 >Create Database   
 > create database if not exists testdatabase;  
 > create database if not exists movie comment "This database holds all tables about movies";  
 > create database accounting location '/user/cloudera'; 
 > show Database  
 > show databases;  
 > show databases like 'm*';  

### Describe Database  
 >describe database movie;  
 > describe formatted database movie;  

 ### Drop Database  
 > drop database movie;  
 >  drop database movie cascade;  

Hive’a bağlanalım: 

``hive ``  
 testdatabase adında bir veritabanı oluşturalım:   
 ``hive> create database testdatabase comment  “sadece test ediyoruz”; ``

Tüm veritabanlarını görelim:   
 ``hive> show databases; ``  
 testdatabase hakkında daha fazla bilgi alalım:   
``hive> describe database extended testdatabase; ``  
(Not: comment’i ve veritabanının oluşturulduğu yeri görebiliyoruz.) 

 testdatabase veritabanını düşürelim(silelim :) ): 

``hive> drop database testdatabase; ``

 testdatabase veritabanını tekrar oluşturalım, bu sefer yerini biz belirtelim: 

``hive> create database testdatabase comment “sadece test ediyoruz” location ‘/user/cloudera’; ``

``hive> describe database extended testdatabase; ``

test ile başlayan veritabanlarımızı görelim: 

``hive> show databases like ‘test*’; ``

İçinde tablolar olan bir veritabanını düşürmek istersek: 

``hive> drop database testdatabase cascade; ``

## Hive Managed Tables 

Hive’da 2 tür tablo var: 
- Managed Table, 
- External Table 

Aralarında şöyle bir fark var:   
- Managed tabloyu düşürürseniz, HDFS’deki datası da silinmiş oluyor. 

- External tabloyu düşürürseniz, sadece schema düşüyor, data HDFS’de kalmaya devam ediyor. 

- Hive’da tablo oluştururken türünü belirtmezsek managed table olur(default). 

### HIVE MANAGED TABLES KOMUTLARI 

Create Managed Table   

```
CREATE TABLE customers \
(custId INT, fName STRING, lName STRING, city STRING)  \
ROW FORMAT DELIMITED  \
FIELDS TERMINATED BY '|' \  
STORED AS TEXTFILE; 
```
#### Load Data From HDFS Location  

``LOAD DATA INPATH '/user/cloudera/customer-hive ' overwrite into table customers;  ``

#### Load Data From Local File System  

``LOAD DATA LOCAL INPATH '/home/cloudera/customers.csv' overwrite into table ‘customers’;  ``
#### Drop Managed Table  
 ``drop table customers; ``

Hive’da Customers tablosunu oluşturalım: 

``hive>  CREATE TABLE customers  (custId INT, fName STRING, lName STRING, city STRING)  ROW FORMAT DELIMITED  FIELDS TERMINATED BY '|'  STORED AS TEXTFILE; ``


Tablonun yapısını görelim: 

``hive> describe formatted customers; ``

Tabloyu sorgulayıp boş olduğunu görelim: 

``hive> select * from customers; ``

 Tabloya HDFS’den veri yükleyelim: 

( Daha önce yapmadıysak sqoop importla veriyi alalım: )

```
sqoop import --connect "jdbc:mysql://localhost/retail_db" \
--username root --password cloudera \
--table customers \
--target-dir /user/cloudera/customer-hive \
--fields-terminated-by '|' \
–-delete-target-dir \
--columns "customer_id,customer_fname,customer_lname,customer_city"  
```

``hive> LOAD DATA INPATH ‘/user/cloudera/customer_hive’ overwrite into table customers; ``

 ``hive> select * from customers limit 5; ``

Tablonun yapısına bakalım: 

``hive> describe formatted customers; ``

Burada gözüken tablonun location’ını ls komutuyla sorgulayalım: 

``hdfs dfs -ls /user/hive/warehouse/customers ``

Şimdi tabloyu ilk oluşturduğumuz dizine bakalım: 

``hdfs dfs -ls  /user/cloudera/customer_hive ``

** (Burada dizinin silindiğini göreceksiniz. Yani LOAD DATA copy yapmıyor, move yapıyor) **

Tabloyu düşürelim: 

``hive> drop table customers; ``  
 Şimdi tablo location’ını tekrar sorgulayalım: (/user/hive/warehouse/customers)  
 ``hdfs dfs -ls /user/hive/warehouse/customers ``  
(Silindiğini göreceğiz. Managed table silinirse, data dizini de silinir.) 

## Hive External Tables
### Commands
#### Create External Table  

``CREATE EXTERNAL TABLE customers  (custId INT, fName STRING, lName STRING, city STRING)  ROW FORMAT DELIMITED  FIELDS TERMINATED BY '|'  STORED AS TEXTFILE  LOCATION '/user/cloudera/customer-hive'; ``


#### Drop External Table  
``DROP TABLE customers;  ``
#### Create Table  
``CREATE TABLE customer_new as SELECT * from customers; ``  
Hive’da External olarak Customers tablosunu oluşturalım:   
``hive> CREATE EXTERNAL TABLE customers  (custId INT, fName STRING, lName STRING, city STRING)  ROW FORMAT DELIMITED  FIELDS TERMINATED BY '|'  STORED AS TEXTFILE  LOCATION '/user/cloudera/customer-hive';  ``  
 Tablo yapısına bakalım:   
``hive>  describe formatted customers; ``  
 (Not: Location’a dikkat, bizim belirttiğimiz yeri kullanıyor.)   
Location dizinini sorgulayalım:   
``hdfs dfs -ls /user/cloudera/customer-hive ``  
Tablonun verilerine bakalım:   
``hive> select * from customers limit 5; ``  
(Not: Tablo datalarını göreceksiniz. External Table’da data load yapmaya gerek yok.) 
Tabloyu düşürelim:   
``hive> drop table customers; ``  
Tablo location’ını tekrar sorgulayalım:   
``hdfs dfs -ls /user/cloudera/customer-hive ``  
 Görüldüğü gibi datalar aynen duruyor. External table düşürülürken sadece schema düşürülür, data dizinine dokunulmaz.   
## Hive Inserts 

Hive’da subject isimli bir tablo oluşturalım: 

``hive> create external table subject (name String, students INT) ROW FORMAT DELIMITED FIELDS TERMINATED BY ‘,’; ``

 hive’dan çıkıp işletim sisteminde subject.txt isimli bir dosya oluşturalım. İçeriği: 

>Edebiyat,20 Matematik,15 Fen,10 Müzik,5 

hive’da veriyi insert edelim: 

``hive> LOAD DATA LOCAL INPATH ‘/home/cloudera/subject.txt’ into table subject; ``
``hive> select * from subject; ``
``hive> insert into subject values(‘Fizik’,40); ``
``hive> select * from subject; ``

Yeni bir tablo oluşturalım:   
``hive> create external table customer_data (name String, city String); ``

Uygun bir tablodan buraya insert edelim: 

``hive> insert into customer_data select fname, city customer_new; ``

``hive> select * from customer_data; ``
## Hive Analytics 
### GroupBy/OrderBy Function  
``Select city, count(*)  from customers  group by city  order by city  limit 100;  ``
 ### GroupBy/Having/OrderBy Function  
``Select city, count(*)  from customers  group by city  having count(*)>200  order by city  limit 5;  ``
### IF clause  
``Select city, if (count(*) >50,1,0) as bigCity  from customers  group by city;  ``
### Min/Max/Sum Function  
``Select min(custid),max(custid),sum(custid),avg(custid)  from customers; ``
 ### Different formats/Compressions  
- Parquet Format  
 ``Create table customer_parquet  stored as parquet  location '/user/cloudera/customers-parquet'  as select * from customers;  ``
- Parquet Format with Snappy compression  
 `` SET parquet.compression=SNAPPY;  ``  
 `` SET hive.exec.compress.output=true; ``  
- ``Create table customer_parquet_snappy  stored as parquet  location '/user/cloudera/customer-parquet-snappy'  as select * from customers; ``  

```
sqoop import --connect "jdbc:mysql://localhost/retail_db" --username root --password cloudera --table customers --target-dir /user/cloudera/customer-avro --fields-terminated-by '|' --columns "customer_id,customer_fname,customer_lname,customer_city" --as-avrodatafile  
``` 

``avro-tools getschema hdfs://localhost/user/cloudera/customer-avro/part-m-00000.avro > customer.avsc  ``  

``hdfs dfs -mkdir /user/cloudera/customer-avro-schema  ``  

``hdfs dfs -put customer.avsc /user/cloudera/customer-avro-schema  ``  

 - Avro Format  
 ``CREATE EXTERNAL TABLE customers_avro  (custId INT, fName STRING, lName STRING, city STRING)  ROW FORMAT DELIMITED  FIELDS TERMINATED BY '|'  STORED AS AVRO  location '/user/cloudera/customer-avro'  TBLPROPERTIES ('avro.schema.url'='/user/cloudera/customer-avroschema/customer.avsc'); ``
  
- Fixed File format using Regular Expressions  
 ``Create External Table employee_fixed  (empId int, name STRING, age int)  ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.RegexSerDe'  with SERDEPROPERTIES("input.regex" = "(.{4})(.{10})(.{2})" )  location '/user/cloudera/employee-fixed'; ``

 - Avro Format with Snappy Compressions  
 ``CREATE TABLE customers_avro_snappy  STORED AS AVRO  location '/user/cloudera/customer-avro-snappy'  TBLPROPERTIES ('avro.schema.url'='/user/cloudera/customer-avroschema/customer.avsc', "avro.output.codec"="snappy")  as Select * from customers; 
 ``

# 5 - SAMPLES
## HIVE

```
sqoop import \
--connect jdbc:mysql://localhost/retail_db \
--username root --password cloudera \
--table copy_customers \
--target-dir /user/cloudera/customers_hive \
--fields-terminated-by "|" \
--null-string "alibaba" \
--null-non-string "999" \
--delete-target-dir \
--split-by customer_id \
--hive-import \
--create-hive-table \
--hive-database default \
--hive-table customer_mysql
``` 
``#hive``

```
hive > use default;
hive > select count(*) from customer_mysql;
hive> select count(*) from customer_mysql; 
```
>Query ID = cloudera_20191127012222_85f43243-3e92-43f6-86cd-cdc4f3636e99  
>Total jobs = 1  
>Launching Job 1 out of 1  
>Number of reduce tasks determined at compile time: 1  
>In order to change the average load for a reducer (in bytes):  
>  set hive.exec.reducers.bytes.per.reducer=<number>
>In order to limit the maximum number of reducers:  
>  set hive.exec.reducers.max=<number>  
>In order to set a constant number of reducers:  
>  set mapreduce.job.reduces=<number>  
>Starting Job = job_1574835896347_0014, Tracking URL = http://quickstart.cloudera:8088/proxy/application_1574835896347_0014/  
>Kill Command = /usr/lib/hadoop/bin/hadoop job  -kill job_1574835896347_0014  
>Hadoop job information for Stage-1: number of mappers: 1; number of reducers: 1  
>2019-11-27 01:23:14,855 Stage-1 map = 0%,  reduce = 0%  
>2019-11-27 01:23:22,417 Stage-1 map = 100%,  reduce = 0%, Cumulative CPU 1.75 sec  
>2019-11-27 01:23:29,167 Stage-1 map = 100%,  reduce = 100%, Cumulative CPU 3.43 sec  
>MapReduce Total cumulative CPU time: 3 seconds 430 msec  
>Ended Job = job_1574835896347_0014  
>MapReduce Jobs Launched:   
>Stage-Stage-1: Map: 1  Reduce: 1   Cumulative CPU: 3.43 sec   HDFS Read: 919884 HDFS Write: 6 SUCCESS  
>Total MapReduce CPU Time Spent: 3 seconds 430 msec  
>OK  
>12435  
>Time taken: 32.288 seconds, Fetched: 1 row(s)  

hive arayüzünde impala editöründe:  
``invalidate metadata``   
diyerek yeni tabloları görmesini sağlıyoruz. 

## HIVE Commands
``describe formatted customer_mysql``
```
hive> describe formatted customer_mysql;
OK
# col_name            	data_type           	comment             
	 	 
customer_id         	int                 	                    
customer_fname      	string              	                    
customer_lname      	string              	                    
customer_email      	string              	                    
customer_password   	string              	                    
customer_street     	string              	                    
customer_city       	string              	                    
customer_state      	string              	                    
customer_zipcode    	int                 	                    
	 	 
# Detailed Table Information	 	 
Database:           	default             	 
Owner:              	cloudera            	 
CreateTime:         	Wed Nov 27 01:21:56 PST 2019	 
LastAccessTime:     	UNKNOWN             	 
Protect Mode:       	None                	 
Retention:          	0                   	 
Location:           	hdfs://quickstart.cloudera:8020/user/hive/warehouse/customer_mysql	 
Table Type:         	MANAGED_TABLE       	 
Table Parameters:	 	 
	COLUMN_STATS_ACCURATE	true                
	comment             	Imported by sqoop on 2019/11/27 01:21:52
	numFiles            	4                   
	numRows             	12435               
	rawDataSize         	898581              
	totalSize           	911016              
	transient_lastDdlTime	1574854652          
	 	 
# Storage Information	 	 
SerDe Library:      	org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe	 
InputFormat:        	org.apache.hadoop.mapred.TextInputFormat	 
OutputFormat:       	org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat	 
Compressed:         	No                  	 
Num Buckets:        	-1                  	 
Bucket Columns:     	[]                  	 
Sort Columns:       	[]                  	 
Storage Desc Params:	 	 
	field.delim         	|                   
	line.delim          	\n                  
	serialization.format	|                   
Time taken: 1.236 seconds, Fetched: 42 row(s)
````
``show create table customer_mysql``
```
hive> describe formatted customer_mysql;
OK
# col_name            	data_type           	comment             
	 	 
customer_id         	int                 	                    
customer_fname      	string              	                    
customer_lname      	string              	                    
customer_email      	string              	                    
customer_password   	string              	                    
customer_street     	string              	                    
customer_city       	string              	                    
customer_state      	string              	                    
customer_zipcode    	int                 	                    
	 	 
# Detailed Table Information	 	 
Database:           	default             	 
Owner:              	cloudera            	 
CreateTime:         	Wed Nov 27 01:21:56 PST 2019	 
LastAccessTime:     	UNKNOWN             	 
Protect Mode:       	None                	 
Retention:          	0                   	 
Location:           	hdfs://quickstart.cloudera:8020/user/hive/warehouse/customer_mysql	 
Table Type:         	MANAGED_TABLE       	 
Table Parameters:	 	 
	COLUMN_STATS_ACCURATE	true                
	comment             	Imported by sqoop on 2019/11/27 01:21:52
	numFiles            	4                   
	numRows             	12435               
	rawDataSize         	898581              
	totalSize           	911016              
	transient_lastDdlTime	1574854652          
	 	 
# Storage Information	 	 
SerDe Library:      	org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe	 
InputFormat:        	org.apache.hadoop.mapred.TextInputFormat	 
OutputFormat:       	org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat	 
Compressed:         	No                  	 
Num Buckets:        	-1                  	 
Bucket Columns:     	[]                  	 
Sort Columns:       	[]                  	 
Storage Desc Params:	 	 
	field.delim         	|                   
	line.delim          	\n                  
	serialization.format	|                   
Time taken: 1.236 seconds, Fetched: 42 row(s)
````
````
hive> show create table customer_mysql;
OK
CREATE TABLE `customer_mysql`(
  `customer_id` int, 
  `customer_fname` string, 
  `customer_lname` string, 
  `customer_email` string, 
  `customer_password` string, 
  `customer_street` string, 
  `customer_city` string, 
  `customer_state` string, 
  `customer_zipcode` int)
COMMENT 'Imported by sqoop on 2019/11/27 01:21:52'
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='|', 
  'line.delim'='\n', 
  'serialization.format'='|') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://quickstart.cloudera:8020/user/hive/warehouse/customer_mysql'
TBLPROPERTIES (
  'COLUMN_STATS_ACCURATE'='true', 
  'numFiles'='4', 
  'numRows'='12435', 
  'rawDataSize'='898581', 
  'totalSize'='911016', 
  'transient_lastDdlTime'='1574854652')
Time taken: 0.127 seconds, Fetched: 30 row(s)````
```
