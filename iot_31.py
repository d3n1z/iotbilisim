#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

# Lod the diabetes dataset

diabetes = datasets.load_diabetes()
print(dir(diabetes))
print(diabetes.feature_names)

diabetes_X = diabetes.data[:,2].reshape(-1,1)
#diabetes_X = diabetes.data[:150]
diabetes_X_train = diabetes_X[:-20]
diabetes_X_test = diabetes_X[-20:]
print(diabetes_X_train.ndim)

#scikit altında datasetlerinin target ında adamın ne kadar 
diabetes_y_train = diabetes.target[:-20]
diabetes_y_test = diabetes.target[-20:]

print(diabetes_y_test.ndim)

regr = linear_model.LinearRegression()
print(dir(regr))

regr.fit(diabetes_X_train, diabetes_y_train)

print(regr.coef_)
diabetes_y_predicted = regr.predict(diabetes_X_test)
print(diabetes_y_predicted)
print(mean_squared_error(diabetes_y_test, diabetes_y_predicted))

plt.scatter(diabetes_X_test,diabetes_y_test, color='black')
plt.scatter(diabetes_X_train, diabetes_y_train, color='red')
plt.plot(diabetes_X_test, diabetes_y_predicted, color='blue', linewidth=1)
#plt.plot(diabetes_X_train, diabetes_y_train, color='red', linewidth=1)

plt.savefig('figure3')
