import numpy as np
#np.set_printoptions(threshold=6)

print("a = np.full((3,3),1,dtype=bool)")
a = np.full((3,3),1,dtype=bool)

print(a)

print("#boyu 10 olan bir array oluştur")
print("a=np.arange(10)")
a=np.arange(10)

print("#2 ye bölünebilen değerleri al")
print("a = a[a %2 ==0]")
a = a[a %2 ==0]
print (a)

print("# a yı tekrar oluştur")
print("a=np.arange(10)")
a=np.arange(10)

print("# a nın kopyasını oluştur")
print("b = a.copy()")
b = a.copy()

print("c =  np.copy(a)")
c =  np.copy(a)

print("# 2 ye bölünebilen değerlerin yerine 1 bas")
print("b[b % 2 == 0] = 1")
b[b % 2 == 0] = 1
print("c[c %2 == 0] = 1")
c[c %2 == 0] = 1
print("# b yi print et")
print(b)

print("#c yi print et")
print(c)
print("# a yı print et")
print(a)

print("# intersect olanları filtrele")
print("# random iki array oluştur")
print("a = np.random.randint(0,10,(1,10))")
a = np.random.randint(0,10,(1,10))
print("b = np.random.randint(0,10,(1,10))")
b = np.random.randint(0,10,(1,10))
print(a)
print(b)
print("c = np.intersect1d(a,b)")
c = np.intersect1d(a,b)
print(c)


print("# arrayleri topla")

print("c = a+b")
c = a+b
print(c)

print("# array reshape 5 2")

print("c = c.reshape(5,2)")
c = c.reshape(5,2)

print(c)

print("reorder array ")

print("d = c[[1,4,0,3,2],:]")
d = c[[1,4,0,3,2],:]
print(d)
