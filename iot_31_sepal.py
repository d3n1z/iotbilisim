#!/usr/bin/env python


import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import decomposition
from sklearn import datasets
np.random.seed(5)
centers = [[1,1], [-1,-1], [1,-1]]
iris = datasets.load_iris()
X = iris.data
y = iris.target

# 3 boyutlu islem için cizim objesinin bir instance oluşturuyoruz
fig = plt.figure(1, figsize=(4,3))

#clear figure
plt.clf()
ax = Axes3D(fig, rect=[0,0,.95,1], elev=48, azim=134)
#clear axis
plt.cla()

# 3 componentli bir pca oluşturuyoruz. 
# grafiğimiz 3 boyutlu bu noktalar oraya oturacak 
pca = decomposition.PCA(n_components=3)

print("X.size")
print(X.size)
print("X.ndim")
print(X.ndim)
pca.fit(X)

#principal component analysis  ile baktırıyoruz şimdi 
X2 = pca.transform(X)

for name, label in [('Setosa', 0), ('Versicolor',1), ('Virginica', 2) ]:
    ax.text3D(X[y == label, 0].mean(),
              X[y == label, 1].mean() +1.5,
              X[y == label, 2].mean() , name, horizontalalignment='center',
              bbox=dict(alpha=.5, edgecolor='w', facecolor='w'))


y = np.choose(y, [1, 2, 0]).astype(np.float)
ax.scatter(X[:,0], X[:,1], X[:,2], c=y, cmap=plt.cm.nipy_spectral, edgecolor='k')

plt.savefig('original')
plt.show()

plt.clf()
plt.cla()

for name, label in [('Setosa', 0), ('Versicolor',1), ('Virginica', 2) ]:
    ax.text3D(X2[y == label, 0].mean(),
              X2[y == label, 1].mean() +1.5,
              X2[y == label, 2].mean() , name, horizontalalignment='center',
              bbox=dict(alpha=.5, edgecolor='w', facecolor='w'))

ax.scatter(X2[:,0], X2[:,1], X2[:,2], c=y, cmap=plt.cm.nipy_spectral, edgecolor='k')
plt.show()
plt.savefig('pca')

